#ifndef _H_EDITULTRA_SYMBOLLISTCTL_
#define _H_EDITULTRA_SYMBOLLISTCTL_

#include "framework.h"

int IfSymbolReqularExp_CreateSymbolListCtl( struct TabPage *pnodeTabPage , HFONT hFont );
int ReloadSymbolList_WithReqularExp( struct TabPage *pnodeTabPage );
int GetCurrentWordAndJumpGotoLine( struct TabPage *pnodeTabPage );
int GetSymbolListItemAndJumpGotoLine( struct TabPage *pnodeTabPage );

#endif
