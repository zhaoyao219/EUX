package main

import (
    "fmt"
    "runtime"
    "time"
)

func forever() {
    for {
    }
}

func main() {
    go forever()

    time.Sleep(time.Millisecond)  // 让出执行权
    runtime.GOMAXPROCS(1926)      // 等待 stw
    fmt.Println("Done")           // 永远执行不到
}
